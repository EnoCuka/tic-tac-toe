package tictactoe;

import javax.swing.*;
import java.awt.*;

public class Cell extends JButton {
    private int index = -1;
    private boolean busy = false;

    public Cell(String text, int index) {
        super(text);
        setIndex(index);
        setFont(new Font("Arial", Font.BOLD, 50));
        setBackground(Color.WHITE);
    }

    public Cell(int index) {
        this("", index);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }
}
