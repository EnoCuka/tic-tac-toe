package tictactoe;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class TicTacToe {
    private boolean turnOfPlayerX = true;
    private JLabel infoLabel = null;
    private ArrayList<Cell> cells;

    public ArrayList<Cell> getCells() {
        if (cells == null) {
            cells = new ArrayList<Cell>();
        }
        return cells;
    }

    public void setCells(ArrayList<Cell> cells) {
        this.cells = cells;
    }

    public JLabel getInfoLabel() {
        if (infoLabel == null) {
            infoLabel = new JLabel();
            infoLabel.setHorizontalAlignment(JLabel.CENTER);
            infoLabel.setVerticalAlignment(JLabel.CENTER);
            infoLabel.setFont(new Font("Arial", Font.PLAIN, 30));
        }
        return infoLabel;
    }

    public void setInfoLabel(JLabel infoLabel) {
        this.infoLabel = infoLabel;
    }

    public boolean isTurnOfPlayerX() {
        return turnOfPlayerX;
    }

    public void setTurnOfPlayerX(boolean turnOfPlayerX) {
        this.turnOfPlayerX = turnOfPlayerX;
    }

    private void updateInfoLabel() {
        if (isTurnOfPlayerX()) {
            getInfoLabel().setText("Radhen e ka lojtari X");
        } else {
            getInfoLabel().setText("Radhen e ka lojtari O");
        }
    }


    private JPanel createCenterPanel() {
        getCells().clear();
        JPanel panel = new JPanel(new GridLayout(3, 3));
        panel.setBorder(new EmptyBorder(20, 20, 20, 20));
        panel.setBackground(Color.WHITE);
        for (int i = 0; i < 9; i++) {
            Cell cell = new Cell(i);
            getCells().add(cell);
            setBorderOnCell(cell);
            setActionOnCell(cell);
            panel.add(cell);
        }
        return panel;
    }

    private JPanel createTopPane() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Color.WHITE);
        updateInfoLabel();
        panel.add(getInfoLabel(), BorderLayout.CENTER);
        return panel;
    }

    private JPanel creatMainPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Color.WHITE);
        panel.add(createTopPane(), BorderLayout.PAGE_START);
        panel.add(createCenterPanel(), BorderLayout.CENTER);
        return panel;
    }

    private JFrame createJFrame() {
        JFrame jFrame = new JFrame("Tic Tac Toe");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setSize(500, 500);
        jFrame.setResizable(false);
        jFrame.setContentPane(creatMainPanel());
        return jFrame;
    }

    private void setActionOnCell(final Cell cell) {
        cell.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!cell.isBusy()) {
                    if (isTurnOfPlayerX()) {
                        cell.setText("X");
                        cell.setForeground(Color.RED);
                    } else {
                        cell.setText("O");
                        cell.setForeground(Color.BLUE);
                    }
                    cell.setBusy(true);
                    setTurnOfPlayerX(!isTurnOfPlayerX());
                    updateInfoLabel();
                    String winnerName = getWinnerName();
                    if (!winnerName.isEmpty()) {
                        getInfoLabel().setText("Fituesi eshte " + winnerName + " !");
                        setAllCellsBusy();
                    } else if (areAllCellsBusy()) {
                        getInfoLabel().setText("Loja mbaroi barazim !");
                    }
                }
            }
        });
    }

    private String getWinnerName() {
        if (!getCells().get(0).getText().isEmpty() && getCells().get(0).getText().equals(getCells().get(1).getText()) && getCells().get(0).getText().equals(getCells().get(2).getText())) {
            return getCells().get(0).getText();
        } else if (!getCells().get(3).getText().isEmpty() && getCells().get(3).getText().equals(getCells().get(4).getText()) && getCells().get(3).getText().equals(getCells().get(5).getText())) {
            return getCells().get(3).getText();
        } else if (!getCells().get(6).getText().isEmpty() && getCells().get(6).getText().equals(getCells().get(7).getText()) && getCells().get(6).getText().equals(getCells().get(8).getText())) {
            return getCells().get(6).getText();
        } else if (!getCells().get(0).getText().isEmpty() && getCells().get(0).getText().equals(getCells().get(3).getText()) && getCells().get(0).getText().equals(getCells().get(6).getText())) {
            return getCells().get(0).getText();
        } else if (!getCells().get(1).getText().isEmpty() && getCells().get(1).getText().equals(getCells().get(4).getText()) && getCells().get(1).getText().equals(getCells().get(7).getText())) {
            return getCells().get(1).getText();
        } else if (!getCells().get(2).getText().isEmpty() && getCells().get(2).getText().equals(getCells().get(5).getText()) && getCells().get(2).getText().equals(getCells().get(8).getText())) {
            return getCells().get(2).getText();
        } else if (!getCells().get(0).getText().isEmpty() && getCells().get(0).getText().equals(getCells().get(4).getText()) && getCells().get(0).getText().equals(getCells().get(8).getText())) {
            return getCells().get(0).getText();
        } else if (!getCells().get(2).getText().isEmpty() && getCells().get(2).getText().equals(getCells().get(4).getText()) && getCells().get(2).getText().equals(getCells().get(6).getText())) {
            return getCells().get(2).getText();
        }
        return "";
    }

    private boolean areAllCellsBusy() {
        for (Cell cell : getCells()) {
            if (!cell.isBusy()) {
                return false;
            }
        }
        return true;
    }

    private void setAllCellsBusy() {
        for (Cell cell : getCells()) {
            cell.setBusy(true);
        }
    }

    private void setBorderOnCell(Cell cell) {
        int index = cell.getIndex();
        int borderSize = 5;
        if (index == 0) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    0, 0, borderSize, borderSize, Color.gray));
        } else if (index == 1) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    0, borderSize, borderSize, borderSize, Color.gray));
        } else if (index == 2) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    0, borderSize, borderSize, 0, Color.gray));
        } else if (index == 3) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    borderSize, 0, borderSize, borderSize, Color.gray));
        } else if (index == 4) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    borderSize, borderSize, borderSize, borderSize, Color.gray));
        } else if (index == 5) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    borderSize, borderSize, borderSize, 0, Color.gray));
        } else if (index == 6) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    borderSize, 0, 0, borderSize, Color.gray));
        } else if (index == 7) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    borderSize, borderSize, 0, borderSize, Color.gray));
        } else if (index == 8) {
            cell.setBorder(BorderFactory.createMatteBorder(
                    borderSize, borderSize, 0, 0, Color.gray));
        }
    }

    public void showJFrame() {
        createJFrame().setVisible(true);
    }
}
